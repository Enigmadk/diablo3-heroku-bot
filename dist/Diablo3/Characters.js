"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Diablo3 Class Slugs
 * @return enum
 */
var Class;
(function (Class) {
    Class["Barbarian"] = "barbarian";
    Class["Crusader"] = "crusader";
    Class["DemonHunter"] = "demon-hunter";
    Class["Monk"] = "monk";
    Class["Necromancer"] = "necromancer";
    Class["WitchDoctor"] = "witch-doctor";
    Class["Wizard"] = "wizard";
})(Class = exports.Class || (exports.Class = {}));
/**
 * Diablo3 Class Icons
 * @return enum
 */
var Icon;
(function (Icon) {
    Icon["Barbarian"] = "https://us.diablo3.com/static/images/hero/barbarian/crest.png";
    Icon["Crusader"] = "https://us.diablo3.com/static/images/hero/crusader/crest.png";
    Icon["DemonHunter"] = "https://us.diablo3.com/static/images/hero/demon-hunter/crest.png";
    Icon["Monk"] = "https://us.diablo3.com/static/images/hero/monk/crest.png";
    Icon["Necromancer"] = "https://us.diablo3.com/static/images/hero/necromancer/crest.png";
    Icon["WitchDoctor"] = "https://us.diablo3.com/static/images/hero/witch-doctor/crest.png";
    Icon["Wizard"] = "https://us.diablo3.com/static/images/hero/wizard/crest.png";
})(Icon = exports.Icon || (exports.Icon = {}));
/**
 * Diablo3 Gender
 * @return enum
 */
var Gender;
(function (Gender) {
    Gender[Gender["Male"] = 0] = "Male";
    Gender[Gender["Female"] = 1] = "Female";
})(Gender = exports.Gender || (exports.Gender = {}));
//# sourceMappingURL=Characters.js.map