"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Discord = require("discord.js");
const D3DHandlers_1 = require("./D3DHandlers");
/**
 * Discord Diablo3 Client
 */
class D3DClient {
    constructor(config) {
        if (typeof config.discord_token === "undefined")
            throw new Error("Please send a valid discord bot token");
        this._token = config.discord_token;
        this.client = new Discord.Client();
        this.handlers = new D3DHandlers_1.D3DHandlers(this.client, config);
        this.initialize();
    }
    /**
     * Initializes Discord Bot
     *
     * @return void
     */
    initialize() {
        this.client.on('ready', () => {
            console.log(`Logged in as ${this.client.user.tag}!`);
        });
        //Register handlers for commands
        this.handlers.initialize();
        //Login the bot
        this.client.login(this._token);
    }
    /**
     * Discord.Client Instance
     *
     * @return Discord.Client
     */
    get clientInstance() {
        return typeof this.client !== 'undefined' ? this.client : null;
    }
}
exports.D3DClient = D3DClient;
//# sourceMappingURL=D3DClient.js.map