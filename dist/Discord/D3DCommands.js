"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Defined commands (not the best way but practical)
 */
exports.D3DCommands = [
    { key: "account", description: "You can retrieve a battle.net information", args: "battlenet#id" },
    { key: "commands", description: "You can get the list of all the existing commands", args: "none" },
    { key: "character", description: "Retrieves a single character and displays the information", args: "battlenet#id, hero_id" },
    { key: "character_items", description: "Retrieves a single character items detailed information", args: "battlenet#id, hero_id" },
    { key: "item", description: "Retrieves a single detailed item with the slug/id", args: "itemSlugAndId" }
];
//# sourceMappingURL=D3DCommands.js.map