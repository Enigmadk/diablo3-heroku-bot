"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Loops trough the enumerator and returns the value of a key if matches
 *
 * @param T Enum
 * @param any Key
 *
 * @return T
 */
function getEnumValueByKey(_enum, _key) {
    for (let [key, value] of Object.entries(_enum)) {
        if (key === _key)
            return value;
    }
    return null;
}
exports.getEnumValueByKey = getEnumValueByKey;
/**
 * Loops trough the enumerator and returns the key of a value if matches
 *
 * @param T _enum
 * @param any _value
 *
 * @return string
 */
function getEnumKeyByValue(_enum, _value) {
    for (let [key, value] of Object.entries(_enum)) {
        if (value === _value)
            return key;
    }
}
exports.getEnumKeyByValue = getEnumKeyByValue;
/**
 * Color structure shortcut
 *
 * @var {enum}
 */
var Color;
(function (Color) {
    Color["RedRgb"] = "#ff0000";
    Color["GreenRgb"] = "#18f500";
    Color[Color["Orange"] = 16753920] = "Orange";
})(Color = exports.Color || (exports.Color = {}));
//# sourceMappingURL=Utility.js.map