"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//@ts-check
const D3DClient_1 = require("./Discord/D3DClient");
const API_1 = require("./Diablo3/API");
const fs = require("fs");
const path = require("path");
/**
 * Configuration object
 * @var {Config}
 */
let _config = {
    discord_token: "",
    discord_prefix: "",
    mashory_key: "",
    region: "",
    locale: ""
};
/**
 * Read the config file and load the misc data
 *
 * @return {void}
 */
fs.readFile(path.join(__dirname, '../src/config.json'), 'utf8', (error, data) => {
    if (error)
        console.log("Error raised: " + error);
    let json = JSON.parse(data);
    if (typeof json != 'undefined') {
        _config.discord_token = process.env.bot_token;
        _config.discord_prefix = json.discord.prefix;
        _config.mashory_key = process.env.mashory_key;
        _config.region = json.discord.region;
        const d3d = new D3DClient_1.D3DClient(_config);
        const d3api = new API_1.API(_config.mashory_key, _config.region, _config.locale);
    }
});
//# sourceMappingURL=index.js.map